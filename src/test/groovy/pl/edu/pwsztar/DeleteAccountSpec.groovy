package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should delete account number #fakeAccountNumber for #user"() {

        given: "the account is created"
            def number = bank.createAccount()
        when: "the account is deleted"
            def result = bank.deleteAccount(fakeAccountNumber)
        then: "check account balance"
            result == BankOperation.ACCOUNT_NOT_EXISTS

        where:
            user   | accountNumber  | fakeAccountNumber
            'John' | 1              | 2
            'Tom'  | 2              | 0
            'Mike' | 3              | 5
            'Todd' | 4              | -1
    }
}
