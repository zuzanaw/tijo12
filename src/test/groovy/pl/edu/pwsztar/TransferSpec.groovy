package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should transfer money from account number #accountNumber to account number #toAccountNumber"() {

        given: "the account is created"
            def number = bank.createAccount()
        when: "the money is transferred"
            def result = bank.transfer(accountNumber,toAccountNumber,fakeAmount)
        then: "check accounts exist and balance sufficient"
            result == true

        where:
            user   | accountNumber  | fakeAccountNumber |toAccountNumber    | fakeAmount
            'John' | 1              | 2                 | 2                 | -4
            'Tom'  | 2              | 0                 | 3                 | 0
            'Mike' | 3              | 5                 | 4                 | -1000
            'Todd' | 4              | -1                | 1                 | 100
    }
}
