package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should return account balance for account number #fakeAccountNumber"() {

        given: "the account is created"
            def number = bank.createAccount()
        when: "the money is transferred"
            def result = bank.accountBalance(fakeAccountNumber)
        then: "check accounts exist and balance sufficient"
            result == BankOperation.ACCOUNT_NOT_EXISTS

        where:
            user   | accountNumber  | fakeAccountNumber |toAccountNumber    | fakeAmount
            'John' | 1              | 2                 | 2                 | -4
            'Tom'  | 2              | 0                 | 3                 | 0
            'Mike' | 3              | 5                 | 4                 | -1000
            'Todd' | 4              | -1                | 1                 | 100
    }
}
