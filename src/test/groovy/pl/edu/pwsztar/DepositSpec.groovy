package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should deposit #fakeamount in account number #accountNumber for #user"() {

        given: "the account is created"
            def number = bank.createAccount()
        when: "the money is deposited"
            def result = bank.deposit(accountNumber,fakeAmount)
        then: "check account exists"
            result == true

        where:
            user   | accountNumber  | fakeAccountNumber | fakeAmount
            'John' | 1              | 2                 | -4
            'Tom'  | 2              | 0                 | 0
            'Mike' | 3              | 5                 | -1000
            'Todd' | 4              | -1                | 100
    }
}
